# Plantilla tec
Plantilla usando python con el framework django

### funcionamiento de la aplicación
Instalar paquetes requirements de python

### funcionamiento de preprocesadores
Instalar paquetes package.json de nodejs

### statics de filemanager 
	python manage.py collectstatic

# File Manger

como está ene l settings.py de MEDIA_ROOT, agregar dentro una nueva carpeta con le nombre 'uploads/'

[django-filebrowser](https://django-filebrowser.readthedocs.io/en/latest/)

Eliminar el codigo en la ruta `/lib/site-packages/filebrowser/templates/filebrowser/include/breadcrumbs.html`, linea 4


    {% if not query.pop %}
        <li><a href="{% url 'admin:index' %}">{% trans "Home" %}</a></li>
    {% endif %}
    

Eliminar el codigo en la ruta `/lib/site-packages/grappelli/temapltes/admin/base.html`, linea 87


	<a href="javascript://" class="user-options-handler grp-collapse-handler {% if request.session.original_user %}grp-switch-user-is-target{% else %}grp-switch-user-is-original{% endif %}"></a>

agregar `{% load staticfiles %}` en la linea 2 de las siguites rutas: 
<br />
<br />
	-`/lib/site-packages/filebrowser/templates/filebrowser/include/filelisting.html` <br />
	-`/lib/site-packages/filebrowser/templates/filebrowser/detail.html` <br />

agregar `{% static 'assets/images/' %}` en las href y los src de las imagenes de las siguientes rutas: 
<br />
<br />
	-`/lib/site-packages/filebrowser/templates/filebrowser/include/filelisting.html` <br />
	-`/lib/site-packages/filebrowser/templates/filebrowser/detail.html` <br />

funcionalidad de copiar imagen al hacer click. Editar en la ruta: `/lib/site-packages/filebrowser/templates/filebrowser/include/filelisting.html`, al final del documento agregando el siguiente codigo:


		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script>
		    $(inicio_copiar_ruta);

		    function inicio_copiar_ruta () {
		        $('.fb_viewlink').on('click', copiar_ruta);
		    }
		    function copiar_ruta (e) {
		        e.preventDefault();
		        var ruta_imagen = $(this).attr('href');
		        var $temp = $("<input>");
		        $("body").append($temp);
		        $temp.val(ruta_imagen).select();
		        document.execCommand('copy');
		        $temp.remove();
		        alert('Ruta de la imagen copiado : "'+ruta_imagen+'"');
		    }
		</script>