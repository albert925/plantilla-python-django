var gulp = require('gulp')
var atImport = require("postcss-import");
var mixins = require('postcss-mixins');
var cssnested = require('postcss-nested');
var conditionals = require('postcss-conditionals');
var simplevars = require('postcss-simple-vars');
var rucksack = require('rucksack-css');
var cssnext = require('postcss-cssnext');
var csswring = require('csswring');
//var postcss = require('postcss-mixins');
var postcss = require('gulp-postcss')
var ts = require("gulp-typescript");
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var gulprefixer = require('gulp-autoprefixer');

gulp.task('postcss', function () {
	var processors = [
		//plugines
		atImport(),
		mixins(),
		cssnested,
		conditionals,
		simplevars,
		rucksack,
		//cssnext ya tiene autoprefixe
		cssnext({browsers: ['> 5%', 'ie 8']}),
		csswring()
	]
	//return gulp.src('./resources/css/*.css')  //exportar por cada archivo diferente o que se crea
	return gulp.src('./resources/postcss/*.css')
		.pipe(postcss(processors))
		.pipe(gulp.dest('./plantilla/plantilla/static/css')) //Ruta al exportar
});

gulp.task('sass', function(){
	return gulp.src('./resources/sass/**/*.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulprefixer({browsers: ['> 5%', 'ie 8']}))
		.pipe(gulp.dest('./plantilla/plantilla/static/css'));
});

gulp.task('ts', function () {
	return gulp.src('./resources/typescript/**/*.ts')
		.pipe(sourcemaps.init())
		.pipe(ts({
			target: "ES5",
			experimentalDecorators: true,
			module: "system",
			noImplicitAny: true,
			removeComments: true,
			preserveConstEnums: true,
			sourceMap: true
		}))
		.pipe(gulp.dest('./plantilla/plantilla/static/js'))
});


gulp.task('watch', function () {
	gulp.watch('./resources/postcss/*.css', ['postcss'])
	gulp.watch('./resources/sass/*.scss', ['sass'])
	gulp.watch('./resources/typescript/**/*.ts', ['ts'])
});

gulp.task('default', ['watch']);