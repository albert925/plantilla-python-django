const ANCHO:number = window.innerWidth;
const ALTO:number = window.innerHeight;

class AlertaSwal {
	fondo:string;
	texto:string;
	titulo:string;
	tipo:string;
	html:string;
	close:any;
	cancel:any;
	colorCancel:string;
	classCancel:string;
	textCancel:string;
	colorConfirm:string;
	classConfirm:string;
	textConfirm:string;
	imagen:string;
	background:string;

	constructor() {
		this.fondo = '';
		this.texto = '';
		this.titulo = '';
		this.tipo = '';
		this.html = '';
		this.close = false;
		this.cancel = false;
		this.colorCancel = '#d33';
		this.classCancel = 'btn btn-danger';
		this.textCancel = 'Cancelar';
		this.colorConfirm = '#3085d6';
		this.classConfirm = 'btn btn-success';
		this.textConfirm = 'Ok';
		this.imagen = '';
		this.background = '';
	}

	get(Callback:any){
		swal({
			title: this.titulo, 
			text: this.texto, 
			type: this.tipo, 
			html: this.html,
			imageUrl: this.imagen,
			background: this.background,
			showCloseButton: this.close,
			showCancelButton: this.cancel, 
			cancelButtonColor: this.colorCancel,
			cancelButtonClass: this.classCancel,
			cancelButtonText: this.textCancel,
			confirmButtonColor: this.colorConfirm,
			confirmButtonClass: this.classConfirm,
			confirmButtonText: this.textConfirm
		})
		.then(Callback)
	}

	getTimer(time:number, Callback:any){
		swal({
			title: this.titulo, 
			text: this.texto, 
			type: this.tipo, 
			html: this.html,
			imageUrl: this.imagen,
			background: this.background,
			timer: time,
			showCloseButton: this.close,
			showCancelButton: this.cancel, 
			cancelButtonColor: this.colorCancel,
			cancelButtonClass: this.classCancel,
			cancelButtonText: this.textCancel,
			confirmButtonColor: this.colorConfirm,
			confirmButtonClass: this.classConfirm,
			confirmButtonText: this.textConfirm
		})
		.then(Callback)
	}

	simple(Callback:any){
		swal({
			title: this.titulo, 
			text: this.texto, 
			type: this.tipo, 
			html: this.html,
			imageUrl: this.imagen,
			background: this.background,
			showCloseButton: this.close
		})
		.then(Callback)
		.catch(swal.noop)
	}

	simpleTimer(time:number){
		swal({
			title: this.titulo, 
			text: this.texto, 
			type: this.tipo, 
			html: this.html,
			imageUrl: this.imagen,
			background: this.background,
			showCloseButton: this.close,
			timer: time
		})
	}
}

class Distractor {

	html:string;
	div:any;
	color:string;
	
	constructor(element) {
		this.html = `<div class="cs-loader">
			<div class="cs-loader-inner">
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			</div>
		</div>`;
		this.div = element;
		this.color = '#000';
	}

	show(){
		$('.cs-loader').css({color: this.color});
		this.div.html(this.html);
		this.div.fadeIn();
	}

	hide(){
		$('.cs-loader').css({color: this.color});
		this.div.html(this.html);
		this.div.fadeOut();
	}
}

class Sos {
	windows:any;
	mac:any;
	X11:any;
	linux:any;
	movild:any;

	constructor() {
		this.windows = navigator.appVersion.indexOf("Win")!=-1;
		this.mac = navigator.appVersion.indexOf("Mac")!=-1;
		this.X11 = navigator.appVersion.indexOf("X11")!=-1;
		this.linux = navigator.appVersion.indexOf("Linux")!=-1;
	}

	movil(){
		let userAgent = navigator.userAgent || navigator.vendor || window.opera;
		if (userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i )) {
			this.movild = 'iOS';
		}
		else if (userAgent.match( /Android/i )) {
			this.movild = 'Android';
		}

		return {
			iOS: (this.movild == 'iOS') ? true : false,
			android: (this.movild == 'Android') ? true : false
		};
	}

	Pc(){
		return {
			windows: (this.windows) ? true : false,
			mac: (this.mac) ? true : false,
			linux: (this.linux) ? true : false,
			x11: (this.X11) ? true : false
		};
	}
}

class MensajeErrors {
	div:any;
	input:any;
	classes:string;
	
	constructor() {
		this.input = $('input');
		this.div = this.input.parent();
		this.classes = '';
	}

	add(texto:string){
		this.div.find('.invalid-feedback').remove();
		this.input.addClass('is-invalid');
		this.div.append(`<div class="invalid-feedback open ${this.classes}">${texto}</div>`);
	}

	remove(){
		this.div.find('.invalid-feedback').remove();
		this.input.removeClass('is-invalid');
	}
}

function validateFile(extensions:any, fileExtension:any) {
	let valido = false;
	for(let i= 0; i < extensions.length; i++) {
		if(extensions[i] == fileExtension || extensions[i].toUpperCase() == fileExtension)
			valido = true;
	}
	return valido;	
}

function validateFileSize(inputName:any, megabytes:any) {
	let valido = true;
	
	let maxBytes = 1048576 * megabytes;
	let fileSize = document.getElementById(inputName).files[0].size;
	if(fileSize > maxBytes){
		valido = false;
	}
		
	return valido;
}

function urlFixer(url:any) {
	let fixedUrl = url.replace(/[^A-Za-z0-9 ]/g,'');
	fixedUrl = fixedUrl.replace(/\s{2,}/g,' ');
	fixedUrl = fixedUrl.replace(/\s/g, "-");
	fixedUrl = fixedUrl.toLowerCase();
	
	return fixedUrl;
}

function validteEmptyValue(formItem:any) {
	if(formItem.value == ""){
		formItem.value = "0";
	}
	return validateNumber(formItem.value);
}

function validateNumber(value:any) {
	if(value.indexOf(".") > 0){
		return false;
	}
	else{
		return !isNaN(parseInt(value)) && isFinite(value);
	}
}

function validateEmail(email:string) { 
	let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function validateDate(date:any) {
	let pattern =/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
	return pattern.test(date);
}

function validateTime(time:any) {
	let pattern =/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
	return pattern.test(time);
}

function fixPrice(price:number) {
	return price.replace(/\./g, "");
}

function browsers () {
	let chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	let firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	let opera = navigator.userAgent.toLowerCase().indexOf('opera');
	let ie = navigator.userAgent.indexOf("MSIE") > -1;

	return{
		chrome: (chrome) ? true :  false,
		firefox: (firefox) ? true :  false,
		opera: (opera) ? true :  false,
		explore: (ie) ? true :  false
	};
}

class Resize {
	
	width:number;
	height:number;

	constructor(width?:number, height?:number) {
		this.width = width;
		this.height = height;
	}

	heightPorcentaje(element:any){
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * ALTO) / 100;
		element.css({height: `${total}px`});
	}

	heightDivPadre(element:any){
		let altoPadre = element.parent().css('height');
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * altoPadre) / 100;
		element.css({height: `${total}px`});
	}

	widthPorcentaje(element:any){
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * ANCHO) / 100;
		element.css({width: `${total}px`});
	}

	widthDivPadre(element:any){
		let anchoPadre = element.parent().css('width');
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * anchoPadre) / 100;
		element.css({width: `${total}px`});
	}

	leftRigthWindow(element:any, posicion?:string){
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * ANCHO) / 100;
		let posic:string = (posicion === undefined) ? 'left': posicion;
		element.css({posic: `${total}px`});
	}

	leftRigthDivPadre(element:any, posicion?:string){
		let anchoPadre = element.parent().css('width');
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * anchoPadre) / 100;
		let posic:string = (posicion === undefined) ? 'left': posicion;
		element.css({posic: `${total}px`});
	}

	topBottomWindow(element:any, posicion?:string){
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * ALTO) / 100;
		let posic:string = (posicion === undefined) ? 'top': posicion;
		element.css({posic: `${total}px`});
	}

	topBottomDivPadre(element:any, posicion?:string){
		let altoPadre = element.parent().css('height');
		let porcentaje:number = parseInt(element.attr('data-porcentaje'));
		let total:number = (porcentaje * altoPadre) / 100;
		let posic:string = (posicion === undefined) ? 'top': posicion;
		element.css({posic: `${total}px`});
	}

	get(){
		return {
			height: this.height,
			width: this.width
		};
	}

	resizeWindow(element:any){
		element.css({height: ALTO, width: ANCHO});
	}
}