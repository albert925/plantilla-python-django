$(inicio_login);

function inicio_login () {
	$('#forget-password').on('click', abrirRecordar);
	$('.btncancelpass').on('click', cerrarRecordar);
	validacionFormulariosLogin();
}

function abrirRecordar (e) {
	e.preventDefault();
	$('.recordar-pass').addClass('open');
}

function cerrarRecordar (e) {
	e.preventDefault();
	$('.recordar-pass').removeClass('open');
}

function validacionFormulariosLogin() {
	let tokenIn = $('input[name=csrfmiddlewaretoken]').val();
	$('#login').ajaxForm({
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		beforeSubmit: validarLogin,
		success: respuestaLogin
	});
	$('#emailpass').ajaxForm({
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		beforeSubmit: validarEmailreset,
		success: respuestaEmailreset
	});
}

function validarLogin (formData, jqForm, options) {
	let form = jqForm[0];
	let alerta = new AlertaSwal();

	let usuario:string = form.username.value;
	let password:string = form.password.value;

	if (usuario == '') {
		alerta.titulo = 'Error';
		alerta.texto = 'Ingrese el nombre de usuario';
		alerta.tipo = 'warning';
		alerta.simple();
		console.log(alerta);
		return false;
	}
	else if (password == '') {
		alerta.titulo = 'Error';
		alerta.texto = 'Ingrese la contraseña';
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else{
		$('.loading-adming').addClass('open');
		return true;
	}
}

function validarEmailreset (formData, jqForm, options) {
	let form = jqForm[0];
	let alerta = new AlertaSwal();

	let email:string = form.email.value;

	if (email == '' || email == undefined) {
		alerta.titulo = 'Error';
		alerta.texto = 'Ingrese un correo electronico';
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else if (!validateEmail(email)) {
		alerta.titulo = 'Error';
		alerta.texto = 'Ingrese un correo electronico válido';
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else{
		$('.loading-adming').addClass('open');
		return true;
	}
}

function respuestaLogin (res, statusText, xhr, jqForm) {
	let alerta = new AlertaSwal();
	$('.loading-adming').removeClass('open');
	if (res.boolean) {
		window.location.href='/manage/home';
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
	console.log(res);
}

function respuestaEmailreset (res) {
	let alerta = new AlertaSwal();
	$('.loading-adming').removeClass('open');
	if (res.boolean) {
		$('.recordar-pass').removeClass('open');
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
}