$(inicio_perfil);

function inicio_perfil () {
	validacionFormulariosPerfil();
}

function validacionFormulariosPerfil () {
	let tokenIn = $('input[name=csrfmiddlewaretoken]').val();
	$('#passperfil').ajaxForm({
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		beforeSubmit: validarLogin,
		success: respuestaLogin
	});
}

function validarLogin (formData, jqForm, options) {
	let form = jqForm[0];
	let alerta = new AlertaSwal();
	let error = '';

	let passActual:string = form.passwordactual.value;
	let passNuevo:string = form.passwordnuevo.value;
	let passNueboB:string = form.passwordnuevob.value;

	if (passActual == '') {
		error += '- Ingrese la contraseña actual <br>';
	}
	if (passNuevo == '') {
		error += '- Ingrese la contraseña nueva <br>';
	}
	if (passNueboB == '') {
		error += '- Profavor repite la contraseña <br>';
	}
	if (passNuevo != passNueboB) {
		error += '- Las contraseñas no coinciden <br>';
	}

	if (error != '') {
		alerta.titulo = 'Error';
		alerta.html = error;
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else{
		return true;
	}
}

function respuestaLogin (res, statusText, xhr, jqForm) {
	console.log(res);
	let alerta = new AlertaSwal();
	if (res.code == 0) {
		alerta.titulo = '';
		alerta.texto = res.error;
		alerta.tipo = 'success';
		alerta.simple();
		$('input[type=password]').val('');
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
}