$(inicio_main);

function inicio_main() {
	let windowResize = new Resize();
	console.log(windowResize);

	$('.delete-datos').on('click', eliminarDatos);
	$('.custom-file-input').on('change', colocarNombreRutaArchivo)
}

function eliminarDatos (e) {
	e.preventDefault();
	let alerta = new AlertaSwal();
	let url = $(this).attr('href');
	let id = $(this).attr('data-id');
	let token = $('#tokendiv input[name=csrfmiddlewaretoken]').val();

	console.log(id);
	console.log(url);

	alerta.titulo = 'Advertencia';
	alerta.texto = 'Estás seguro de eliminar el dato?';
	alerta.tipo = 'warning';
	alerta.cancel = true;
	alerta.textConfirm = 'Aceptar';
	alerta.textCancel = 'Cancelar';
	alerta.get(result => {
		if (result.value) {
			$.ajax({
				headers:{
					'X-CSRF-TOKEN': token
				},
				url: url,
				type: 'POST',
				data: {csrfmiddlewaretoken: token, id: id},
				beforeSend: () => $('.loading-adming').addClass('open'),
				success: respuestEliminar(id),
				error: (err) => console.log(err)
			});
		}
	});

	console.log(url, id);
}

function respuestEliminar (id) {
	return function (res) {
		$('.loading-adming').removeClass('open');
		console.log(res);
		let alerta = new AlertaSwal();
		if (res.boolean) {
			//$(`$fila-anime${id}`).remove();
			alerta.titulo = 'Eliminado';
			alerta.texto = res.result;
			alerta.tipo = 'success';
			alerta.textConfirm = 'Aceptar';
			alerta.get(() => location.reload());
		}
	}
}

function colocarNombreRutaArchivo () {
	 $(this).next('.form-control-file').addClass("selected").html($(this).val());
}