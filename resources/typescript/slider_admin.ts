$(inicio_slider);

function inicio_slider () {
	//$('#enviar').on('click', enviarSlider);
	validacionSliderAdmin();
	show_table();
}

function validacionSliderAdmin () {
	let tokenIn:any = $('input[name=csrfmiddlewaretoken]').val();
	$('#sliderForm').ajaxForm({
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		beforeSubmit: enviarSlider,
		success: respuestaSlidder
	});
}

function enviarSlider (formData, jqForm, options) {
	let form = jqForm[0];
	let alerta = new AlertaSwal();

	let titulo:string = $('#titulo').val();
	let tipo:string = $('#tipo').val();
	let actiontype:string = $('#actiontype').val();
	let imagendate:any = $('#imagenVisualizacion img').attr('src');
	let imagen:any = $('#imagen').val();

	let error = '';

	if (actiontype == 'add') {
		if (titulo == '' || titulo == undefined) {
			error += '- Por favor ingrese el tituo de slider <br>';
		}

		if (imagen == undefined || imagen == '') {
			error += '- Por favor selecione la imagen <br>';
		}
	}
	else if (actiontype == 'edit') {
		let id:number = $('#id').val();
		if (id == undefined) {
			error += '- Id slider no disponible <br>';
		}
		if (titulo == '' || titulo == undefined) {
			error += '- Por favor ingrese el tituo de slider <br>';
		}
	}
	else{
		error += '- action type no definido'
	}

	if (error != '') {
		alerta.titulo = 'Error';
		alerta.html = error;
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else{
		$('.loading-adming').addClass('open');
		return true;
	}
}

function respuestaSlidder (res, statusText, xhr, jqForm) {
	$('.loading-adming').removeClass('open');
	console.log(res);
	let alerta = new AlertaSwal();
	if (res.boolean) {
		alerta.titulo = '';
		alerta.texto = res.result;
		alerta.tipo = 'success';
		alerta.simple(() => window.location.href='/manage/slider/');
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
}

function show_table () {
	if ($('#data-table').length !== 0) {
		$('#data-table').DataTable({
			lengthChange: false,
			iDisplayLength: 12,
			language: {
				zeroRecords: ' ',
				info: 'Página _PAGE_ de _PAGES_',
				infoEmpty: ' ',
				infoFiltered: '(Filtrando de _MAX_ total)',
				search: 'Buscador',
				paginate: {
					first : 'Primera',
					last : 'Ultima',
					next : 'Siguiente',
					previous : 'Anterior'
				}
			},
			responsive: true
		});
	}
}