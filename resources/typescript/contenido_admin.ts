$(inicio_Contenidos);

function inicio_Contenidos () {
	//$('#enviar').on('click', enviarSlider);
	validacionContenidoAdmin();
	show_table();
	ordernarContenidos();
}

function validacionContenidoAdmin () {
	let tokenIn:any = $('input[name=csrfmiddlewaretoken]').val();
	$('#sliderForm').ajaxForm({
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		beforeSubmit: enviarContenido,
		success: respuestaContenido
	});
}

function enviarContenido (formData, jqForm, options) {
	let form = jqForm[0];
	let alerta = new AlertaSwal();

	let titulo:string = $('#titulo').val();
	let tipo:string = $('#tipo').val();
	let actiontype:string = $('#actiontype').val();
	let imagendate:any = $('#imagenVisualizacion img').attr('src');
	let imagen:any = $('#imagen').val();

	let error = '';

	if (actiontype == 'add') {
		if (titulo == '' || titulo == undefined) {
			error += '- Por favor ingrese el tituo del contenido <br>';
		}
	}
	else if (actiontype == 'edit') {
		let id:number = $('#id').val();
		if (id == undefined) {
			error += '- Id contenido no disponible <br>';
		}
		if (titulo == '' || titulo == undefined) {
			error += '- Por favor ingrese el tituo del contenido <br>';
		}
	}
	else{
		error += '- action type no definido'
	}

	if (error != '') {
		alerta.titulo = 'Error';
		alerta.html = error;
		alerta.tipo = 'warning';
		alerta.simple();
		return false;
	}
	else{
		$('.loading-adming').addClass('open');
		return true;
	}
}

function respuestaContenido (res, statusText, xhr, jqForm) {
	$('.loading-adming').removeClass('open');
	console.log(res);
	let alerta = new AlertaSwal();
	if (res.boolean) {
		alerta.titulo = '';
		alerta.texto = res.result;
		alerta.tipo = 'success';
		alerta.simple(() => window.location.href='/manage/contenidos/');
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
}

function show_table () {
	if ($('#data-table').length !== 0) {
		$('#data-table').DataTable({
			lengthChange: false,
			iDisplayLength: 12,
			language: {
				zeroRecords: ' ',
				info: 'Página _PAGE_ de _PAGES_',
				infoEmpty: ' ',
				infoFiltered: '(Filtrando de _MAX_ total)',
				search: 'Buscador',
				paginate: {
					first : 'Primera',
					last : 'Ultima',
					next : 'Siguiente',
					previous : 'Anterior'
				}
			},
			responsive: true
		});
	}
}

function ordernarContenidos () {
	$('#mainTable tbody').sortable({
		beforeStop: beforeContenidos
	});
}

function beforeContenidos (event:any, ui:any) {
	$("#mainTable tbody").sortable("disable");

	console.log(ui.item);

	var ordenArray = new Array();

	let idorden = $(ui.item).children('input').attr('data-rowid');
	let posicion = $(ui.item).children('input').attr('data-orden');
	let newPosicion = ui.item.index() + 1;
	let idcon = $(ui.item).attr('data-id');
	let tokenIn:any = $('meta[name=csrf-token]').attr('content');

	$.ajax({
		url: '/admin/posicioncontenidos',
		method: 'POST',
		headers:{
			'X-CSRF-TOKEN': tokenIn
		},
		data:{
			idorden: idorden,
			posicion: posicion,
			newposicion: newPosicion,
			idcon: idcon
		},
		beforeSend: () => $('.loading-adming').addClass('open'),
		success: respuestaOrden
	});
}

function respuestaOrden (res) {
	let alerta = new AlertaSwal();
	$('.loading-adming').removeClass('open');
	console.log(res);

	if (res.boolean) {
		res.result.forEach(date => {
			$(`#fila-anime${date.id}`).attr('data-orden', date.orden);
			$(`#fila-anime${date.id}`).children('input').attr('data-orden', date.orden);
			$(`#fila-anime${date.id} #numorden`).html(`<i class="fa fa-sort"></i>  ${date.orden}`);
		});

		$("#alertInline").hide().stop(true, true);
		$("#alertInline").slideToggle(500).delay(2000).slideToggle(600);
		$("#mainTable tbody").sortable("enable");
	}
	else{
		alerta.titulo = 'Error';
		alerta.texto = res.error;
		alerta.tipo = 'warning';
		alerta.simple();
	}
}