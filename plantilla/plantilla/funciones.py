from collections import namedtuple
from plantilla.validaciones import ValidacionesM
import re

class IsValidM():

	def __init__(self, request, tipo, *args, **kwargs):
		self._request = request
		self._tipo = tipo
		self._messages = []
		self._validaciones = ValidacionesM.validar()
		self._prueba = ''

		for tipo_key, tipo_value in self._tipo.items():
			validacion_tipo = tipo_value.split('|')

			for i in range(0, (len(validacion_tipo))):
				if validacion_tipo[i][3] == ':':
					maximo = int(validacion_tipo[i].split(':')[1])
					if len(request[tipo_key]) > maximo:
						self._messages.append('{} debe tener mínimo de {} caracteres'.format(tipo_key, maximo))
				elif validacion_tipo[i] == 'required':
					if request[tipo_key] is None or request[tipo_key] == '':
						self._messages.append('{} es requerido'.format(tipo_key))
				else:
					for validar_key, validar_value in self._validaciones.items():
						if validar_key == validacion_tipo[i]:
							validar_exp_messages = self._validaciones[validacion_tipo[i]]
							self._prueba = request[tipo_key]
							patron = re.search(validar_exp_messages['exp'], request[tipo_key])
							if not patron:
								self._messages.append(validar_exp_messages['messages'].format(tipo_key))

	def errors(self):
		return self._messages

	def is_valid(self):
		if len(self._messages) > 0:
			return False
		else:
			return True

	def pruebas(self):
		return self._prueba

def covert_object (data):
	return namedtuple("Object", data.keys())(*data.values())