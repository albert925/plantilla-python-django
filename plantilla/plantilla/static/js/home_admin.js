$(inicio_homeadmin);
function inicio_homeadmin() {
    validacionHomeAdmin();
    show_table();
}
function validacionHomeAdmin() {
    var tokenIn = $('input[name=csrfmiddlewaretoken]').val();
    $('#dataForm').ajaxForm({
        headers: {
            'X-CSRF-TOKEN': tokenIn
        },
        beforeSubmit: validarLogos,
        success: respuestaLogo,
        error: erroresAjax
    });
}
function validarLogos(formData, jqForm, options) {
    var form = jqForm[0];
    var alerta = new AlertaSwal();
    var titulo = $('#titulo').val();
    var urllogo = $('#urllogo').val();
    var actiontype = $('#actiontype').val();
    var error = '';
    if (actiontype == 'add') {
        if (urllogo == '' || urllogo == undefined) {
            error += '- Por favor ingrese la url <br>';
        }
        if (titulo == '' || titulo == undefined) {
            error += '- Por favor ingrese el título del logo <br>';
        }
    }
    else if (actiontype == 'edit') {
        var id = $('#id').val();
        if (id == undefined) {
            error += '- Id grupo no disponible <br>';
        }
        if (urllogo == '' || urllogo == undefined) {
            error += '- Por favor ingrese la url <br>';
        }
        if (titulo == '' || titulo == undefined) {
            error += '- Por favor ingrese el título del logo <br>';
        }
    }
    else {
        error += '- action type no definido';
    }
    if (error != '') {
        alerta.titulo = 'Error';
        alerta.html = error;
        alerta.tipo = 'warning';
        alerta.simple();
        return false;
    }
    else {
        $('.loading-adming').addClass('open');
        return true;
    }
}
function respuestaLogo(res, statusText, xhr, jqForm) {
    $('.loading-adming').removeClass('open');
    console.log(res);
    var alerta = new AlertaSwal();
    if (res.boolean) {
        alerta.titulo = '';
        alerta.texto = res.result;
        alerta.tipo = 'success';
        alerta.simple(function () { return window.location.href = '/admin/home-logos/'; });
    }
    else {
        if (res.code == 10) {
            var errorDatos = '';
            for (var i = 0; i < res.error.length; ++i) {
                errorDatos += res.error[i] + " <br>";
            }
            alerta.titulo = 'Error';
            alerta.html = errorDatos;
            alerta.tipo = 'warning';
            alerta.simple();
        }
        else {
            alerta.titulo = 'Error';
            alerta.texto = res.error;
            alerta.tipo = 'warning';
            alerta.simple();
        }
    }
}
function erroresAjax(err) {
    var alerta = new AlertaSwal();
    $('.loading-adming').removeClass('open');
    console.log(err);
    alerta.titulo = "Error";
    alerta.texto = 'Hubo un problema momentáneo con tu conexión a internet, verifícala e intenta de nuevo.';
    alerta.tipo = 'error';
    alerta.simple();
}
function show_table() {
    if ($('#data-table').length !== 0) {
        $('#data-table').DataTable({
            lengthChange: false,
            iDisplayLength: 12,
            language: {
                zeroRecords: ' ',
                info: 'Página _PAGE_ de _PAGES_',
                infoEmpty: ' ',
                infoFiltered: '(Filtrando de _MAX_ total)',
                search: 'Buscador',
                paginate: {
                    first: 'Primera',
                    last: 'Ultima',
                    next: 'Siguiente',
                    previous: 'Anterior'
                }
            },
            responsive: true
        });
    }
}
