$(inicio_slider);
function inicio_slider() {
    validacionSliderAdmin();
    show_table();
}
function validacionSliderAdmin() {
    var tokenIn = $('input[name=csrfmiddlewaretoken]').val();
    $('#sliderForm').ajaxForm({
        headers: {
            'X-CSRF-TOKEN': tokenIn
        },
        beforeSubmit: enviarSlider,
        success: respuestaSlidder
    });
}
function enviarSlider(formData, jqForm, options) {
    var form = jqForm[0];
    var alerta = new AlertaSwal();
    var titulo = $('#titulo').val();
    var tipo = $('#tipo').val();
    var actiontype = $('#actiontype').val();
    var imagendate = $('#imagenVisualizacion img').attr('src');
    var imagen = $('#imagen').val();
    var error = '';
    if (actiontype == 'add') {
        if (titulo == '' || titulo == undefined) {
            error += '- Por favor ingrese el tituo de slider <br>';
        }
        if (imagen == undefined || imagen == '') {
            error += '- Por favor selecione la imagen <br>';
        }
    }
    else if (actiontype == 'edit') {
        var id = $('#id').val();
        if (id == undefined) {
            error += '- Id slider no disponible <br>';
        }
        if (titulo == '' || titulo == undefined) {
            error += '- Por favor ingrese el tituo de slider <br>';
        }
    }
    else {
        error += '- action type no definido';
    }
    if (error != '') {
        alerta.titulo = 'Error';
        alerta.html = error;
        alerta.tipo = 'warning';
        alerta.simple();
        return false;
    }
    else {
        $('.loading-adming').addClass('open');
        return true;
    }
}
function respuestaSlidder(res, statusText, xhr, jqForm) {
    $('.loading-adming').removeClass('open');
    console.log(res);
    var alerta = new AlertaSwal();
    if (res.boolean) {
        alerta.titulo = '';
        alerta.texto = res.result;
        alerta.tipo = 'success';
        alerta.simple(function () { return window.location.href = '/manage/slider/'; });
    }
    else {
        alerta.titulo = 'Error';
        alerta.texto = res.error;
        alerta.tipo = 'warning';
        alerta.simple();
    }
}
function show_table() {
    if ($('#data-table').length !== 0) {
        $('#data-table').DataTable({
            lengthChange: false,
            iDisplayLength: 12,
            language: {
                zeroRecords: ' ',
                info: 'Página _PAGE_ de _PAGES_',
                infoEmpty: ' ',
                infoFiltered: '(Filtrando de _MAX_ total)',
                search: 'Buscador',
                paginate: {
                    first: 'Primera',
                    last: 'Ultima',
                    next: 'Siguiente',
                    previous: 'Anterior'
                }
            },
            responsive: true
        });
    }
}
