var ANCHO = window.innerWidth;
var ALTO = window.innerHeight;
var AlertaSwal = (function () {
    function AlertaSwal() {
        this.fondo = '';
        this.texto = '';
        this.titulo = '';
        this.tipo = '';
        this.html = '';
        this.close = false;
        this.cancel = false;
        this.colorCancel = '#d33';
        this.classCancel = 'btn btn-danger';
        this.textCancel = 'Cancelar';
        this.colorConfirm = '#3085d6';
        this.classConfirm = 'btn btn-success';
        this.textConfirm = 'Ok';
        this.imagen = '';
        this.background = '';
    }
    AlertaSwal.prototype.get = function (Callback) {
        swal({
            title: this.titulo,
            text: this.texto,
            type: this.tipo,
            html: this.html,
            imageUrl: this.imagen,
            background: this.background,
            showCloseButton: this.close,
            showCancelButton: this.cancel,
            cancelButtonColor: this.colorCancel,
            cancelButtonClass: this.classCancel,
            cancelButtonText: this.textCancel,
            confirmButtonColor: this.colorConfirm,
            confirmButtonClass: this.classConfirm,
            confirmButtonText: this.textConfirm
        })
            .then(Callback);
    };
    AlertaSwal.prototype.getTimer = function (time, Callback) {
        swal({
            title: this.titulo,
            text: this.texto,
            type: this.tipo,
            html: this.html,
            imageUrl: this.imagen,
            background: this.background,
            timer: time,
            showCloseButton: this.close,
            showCancelButton: this.cancel,
            cancelButtonColor: this.colorCancel,
            cancelButtonClass: this.classCancel,
            cancelButtonText: this.textCancel,
            confirmButtonColor: this.colorConfirm,
            confirmButtonClass: this.classConfirm,
            confirmButtonText: this.textConfirm
        })
            .then(Callback);
    };
    AlertaSwal.prototype.simple = function (Callback) {
        swal({
            title: this.titulo,
            text: this.texto,
            type: this.tipo,
            html: this.html,
            imageUrl: this.imagen,
            background: this.background,
            showCloseButton: this.close
        })
            .then(Callback)
            .catch(swal.noop);
    };
    AlertaSwal.prototype.simpleTimer = function (time) {
        swal({
            title: this.titulo,
            text: this.texto,
            type: this.tipo,
            html: this.html,
            imageUrl: this.imagen,
            background: this.background,
            showCloseButton: this.close,
            timer: time
        });
    };
    return AlertaSwal;
}());
var Distractor = (function () {
    function Distractor(element) {
        this.html = "<div class=\"cs-loader\">\n\t\t\t<div class=\"cs-loader-inner\">\n\t\t\t<label>\t\u25CF</label>\n\t\t\t<label>\t\u25CF</label>\n\t\t\t<label>\t\u25CF</label>\n\t\t\t<label>\t\u25CF</label>\n\t\t\t<label>\t\u25CF</label>\n\t\t\t<label>\t\u25CF</label>\n\t\t\t</div>\n\t\t</div>";
        this.div = element;
        this.color = '#000';
    }
    Distractor.prototype.show = function () {
        $('.cs-loader').css({ color: this.color });
        this.div.html(this.html);
        this.div.fadeIn();
    };
    Distractor.prototype.hide = function () {
        $('.cs-loader').css({ color: this.color });
        this.div.html(this.html);
        this.div.fadeOut();
    };
    return Distractor;
}());
var Sos = (function () {
    function Sos() {
        this.windows = navigator.appVersion.indexOf("Win") != -1;
        this.mac = navigator.appVersion.indexOf("Mac") != -1;
        this.X11 = navigator.appVersion.indexOf("X11") != -1;
        this.linux = navigator.appVersion.indexOf("Linux") != -1;
    }
    Sos.prototype.movil = function () {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
            this.movild = 'iOS';
        }
        else if (userAgent.match(/Android/i)) {
            this.movild = 'Android';
        }
        return {
            iOS: (this.movild == 'iOS') ? true : false,
            android: (this.movild == 'Android') ? true : false
        };
    };
    Sos.prototype.Pc = function () {
        return {
            windows: (this.windows) ? true : false,
            mac: (this.mac) ? true : false,
            linux: (this.linux) ? true : false,
            x11: (this.X11) ? true : false
        };
    };
    return Sos;
}());
var MensajeErrors = (function () {
    function MensajeErrors() {
        this.input = $('input');
        this.div = this.input.parent();
        this.classes = '';
    }
    MensajeErrors.prototype.add = function (texto) {
        this.div.find('.invalid-feedback').remove();
        this.input.addClass('is-invalid');
        this.div.append("<div class=\"invalid-feedback open " + this.classes + "\">" + texto + "</div>");
    };
    MensajeErrors.prototype.remove = function () {
        this.div.find('.invalid-feedback').remove();
        this.input.removeClass('is-invalid');
    };
    return MensajeErrors;
}());
function validateFile(extensions, fileExtension) {
    var valido = false;
    for (var i = 0; i < extensions.length; i++) {
        if (extensions[i] == fileExtension || extensions[i].toUpperCase() == fileExtension)
            valido = true;
    }
    return valido;
}
function validateFileSize(inputName, megabytes) {
    var valido = true;
    var maxBytes = 1048576 * megabytes;
    var fileSize = document.getElementById(inputName).files[0].size;
    if (fileSize > maxBytes) {
        valido = false;
    }
    return valido;
}
function urlFixer(url) {
    var fixedUrl = url.replace(/[^A-Za-z0-9 ]/g, '');
    fixedUrl = fixedUrl.replace(/\s{2,}/g, ' ');
    fixedUrl = fixedUrl.replace(/\s/g, "-");
    fixedUrl = fixedUrl.toLowerCase();
    return fixedUrl;
}
function validteEmptyValue(formItem) {
    if (formItem.value == "") {
        formItem.value = "0";
    }
    return validateNumber(formItem.value);
}
function validateNumber(value) {
    if (value.indexOf(".") > 0) {
        return false;
    }
    else {
        return !isNaN(parseInt(value)) && isFinite(value);
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateDate(date) {
    var pattern = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
    return pattern.test(date);
}
function validateTime(time) {
    var pattern = /^([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
    return pattern.test(time);
}
function fixPrice(price) {
    return price.replace(/\./g, "");
}
function browsers() {
    var chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    var opera = navigator.userAgent.toLowerCase().indexOf('opera');
    var ie = navigator.userAgent.indexOf("MSIE") > -1;
    return {
        chrome: (chrome) ? true : false,
        firefox: (firefox) ? true : false,
        opera: (opera) ? true : false,
        explore: (ie) ? true : false
    };
}
var Resize = (function () {
    function Resize(width, height) {
        this.width = width;
        this.height = height;
    }
    Resize.prototype.heightPorcentaje = function (element) {
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * ALTO) / 100;
        element.css({ height: total + "px" });
    };
    Resize.prototype.heightDivPadre = function (element) {
        var altoPadre = element.parent().css('height');
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * altoPadre) / 100;
        element.css({ height: total + "px" });
    };
    Resize.prototype.widthPorcentaje = function (element) {
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * ANCHO) / 100;
        element.css({ width: total + "px" });
    };
    Resize.prototype.widthDivPadre = function (element) {
        var anchoPadre = element.parent().css('width');
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * anchoPadre) / 100;
        element.css({ width: total + "px" });
    };
    Resize.prototype.leftRigthWindow = function (element, posicion) {
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * ANCHO) / 100;
        var posic = (posicion === undefined) ? 'left' : posicion;
        element.css({ posic: total + "px" });
    };
    Resize.prototype.leftRigthDivPadre = function (element, posicion) {
        var anchoPadre = element.parent().css('width');
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * anchoPadre) / 100;
        var posic = (posicion === undefined) ? 'left' : posicion;
        element.css({ posic: total + "px" });
    };
    Resize.prototype.topBottomWindow = function (element, posicion) {
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * ALTO) / 100;
        var posic = (posicion === undefined) ? 'top' : posicion;
        element.css({ posic: total + "px" });
    };
    Resize.prototype.topBottomDivPadre = function (element, posicion) {
        var altoPadre = element.parent().css('height');
        var porcentaje = parseInt(element.attr('data-porcentaje'));
        var total = (porcentaje * altoPadre) / 100;
        var posic = (posicion === undefined) ? 'top' : posicion;
        element.css({ posic: total + "px" });
    };
    Resize.prototype.get = function () {
        return {
            height: this.height,
            width: this.width
        };
    };
    Resize.prototype.resizeWindow = function (element) {
        element.css({ height: ALTO, width: ANCHO });
    };
    return Resize;
}());
