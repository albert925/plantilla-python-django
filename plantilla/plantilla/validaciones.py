class ValidacionesM():
	"""docstring for ValidacionesM"""
	def validar():
		validaciones = {
			'active_url': {
				'messages': '{} no es una URL válida.',
				'exp': "^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$"
			},
			'alpha': {
				'messages': '{} sólo debe contener letras.',
				'exp': "^[a-zA-Z]*$"
			},
			'alpha_dash':{
				'messages': '{} sólo debe contener letras, números y guiones.',
				'exp': "^([A-Z]|[a-z]|-|[0-9]){4,}$"
			},
			'alpha_num':{
				'messages': '{} sólo debe contener letras y números.',
				'exp': "^([A-Z]|[a-z]|[0-9]){4,}$"
			},
			'alpha_spaces':{
				'messages': '{} solo puede contener letras y espacios.',
				'exp': "/^[\pL\s\-]+$/u"
			},
			'alpha_spaces_num':{
				'messages': '{} solo puede contener letras, espacios y números.',
				'exp': "^([A-Z]|[a-z]|-|[0-9]|[ñÑáéíóúÁÉÍÓ]|\s|){4,}$"
			},
			'date':{
				'messages': '{} no es una fecha válida.',
				'exp': "^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{4})$"
			},
			'date_time':{
				'messages': '{} no es una fecha válida.',
				'exp': "^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{4})(\s)([0-1][0-9]|2[0-3])(:)([0-5][0-9])(:)([0-5][0-9])$"
			},
			'email':{
				'messages': '{} no es un correo válido',
				'exp': "/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i"
			},
			'numeric':{
				'messages': '{} debe ser numérico.',
				'exp': "[0-9]"
			}
		}

		return validaciones