from django.urls import path, re_path
from django.contrib.auth.views import logout
from administrador.views import (
	LoginAdmin,
	login_admin,
	HomeAdmin,
	SlidersAdmin,
	NewEditViewSlider,
	post_add_edit,
	post_delete_slider,
	ContenidosAdmin,
	NewEditViewContenido,
	post_add_edit_contenido,
	post_delete_contenido
)

app_name = 'manage'

urlpatterns = [
	path('', LoginAdmin.as_view(), name = 'home'),
	path('login', login_admin, name = 'login'),
	path('logout/', logout, {'next_page': '/manage'}, name='logout'),
	path('home/', HomeAdmin.as_view(), name = 'homeadmin'),
	path('slider/', SlidersAdmin.as_view(), name = 'slider'),
	path('slidernewedit/<actiontype>/', NewEditViewSlider.as_view(), name = 'addeditslider'),
	path('slidernewedit/<actiontype>/<id>/', NewEditViewSlider.as_view(), name = 'addeditslider'),
	path('newslider/', post_add_edit, name = 'postnewslider'),
	path('editslider/', post_add_edit, name = 'posteditslider'),
	path('deleteslider/<id>', post_delete_slider, name = 'postdeleteslider'),
	path('contenidos/', ContenidosAdmin.as_view(), name = 'contenidos'),
	path('contenidonewedit/<actiontype>/', NewEditViewContenido.as_view(), name = 'addeditcontenido'),
	path('contenidonewedit/<actiontype>/<id>/', NewEditViewContenido.as_view(), name = 'addeditcontenido'),
	path('newcontenido/', post_add_edit_contenido, name = 'postnewcontenido'),
	path('editcontenido/', post_add_edit_contenido, name = 'posteditcontenido'),
	path('deletecontenido/<id>', post_delete_contenido, name = 'postdeletecontenido')
]