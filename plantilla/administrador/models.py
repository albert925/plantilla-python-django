from django.db import models

ACTIVO = (
	('si', 'si'),
	('no', 'no')
)

# Create your models here.
class Sliders(models.Model):
	titulo = models.CharField(max_length=255)
	image = models.ImageField()
	activo = models.CharField(max_length = 20, choices = ACTIVO)
	created_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.titulo

class Contenidos(models.Model):
	titulo = models.CharField(max_length=255)
	texto = models.TextField()
	activo = models.CharField(max_length = 20, choices = ACTIVO)
	orden = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)		
		