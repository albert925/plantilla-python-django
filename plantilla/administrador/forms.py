from django import forms

class LoginForm(forms.Form):
	dateusername = forms.TextInput(attrs={
		'size': 10,
		'title': 'Nombre usuario',
		'class': 'form-control placeholder-no-fix',
		'id': 'username',
		'placeholder': 'Username',
		'autocomplete': 'off'
	})

	datepassword = forms.PasswordInput(attrs={
		'title': 'Nombre usuario',
		'class': 'form-control placeholder-no-fix',
		'id': 'password',
		'placeholder': 'Password',
		'autocomplete': 'off'
	})

	username = dateusername.render('username', '')
	password = datepassword.render('password', '')