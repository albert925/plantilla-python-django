from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.auth import authenticate, login
from django.views.generic import View, TemplateView
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from administrador.models import Contenidos, Sliders

from administrador.forms import LoginForm
from plantilla.funciones import IsValidM, covert_object

import json
import os

# Create your views here.
class LoginAdmin(View):
	def get(self, request, *args, **kwargs):
		form = LoginForm(request.POST or None)
		print(form)
		return render(request, 'administrador/home.html', {'form': form})

def login_admin(request):
	if request.method == 'POST':
		validar = IsValidM(request.POST, 
			{
				'username': 'required|alpha_dash',
				'password': 'required'
			}
		)
		if validar.is_valid():
			user = authenticate(username=request.POST['username'], password=request.POST['password'])
			if not user:
				respuesta = {
					'code': 20,
					'boolean': False,
					'error': 'Datos incorrectos',
					'result': ''
				}
			elif user.is_superuser == 1:
				login(request, user)
				respuesta = {
					'code': 0,
					'boolean': True,
					'error': '',
					'result': ''
				}
			else:
				respuesta = {
					'code': 21,
					'boolean': False,
					'error': 'Datos incorrectos',
					'result': ''
				}
		else:
			respuesta = {
				'code': 10,
				'boolean': False,
				'error': validar.errors(),
				'result': ''
			}

		return JsonResponse(respuesta, safe=False)
	else:
		return redirect('home')

class HomeAdmin(View):
	def get(self, request,*args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('/manage')
		else:
			return render(request, 'administrador/inicio.html')

class SlidersAdmin (View):
	def get(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('/manage')
		else:
			imagenes = Sliders.objects.all()
			return render(request, 'administrador/sliders/home.html', {'imagenes': imagenes})

class NewEditViewSlider(View):
	def get(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('/manage')

		actiontype = kwargs['actiontype']

		if 'id' in kwargs:
			idR = kwargs['id']
			datos = Sliders.objects.get(id=idR)
		else:
			idR = 0
			datos = None

		if actiontype == 'add':
			texto_action = 'nuevo'
		elif actiontype == 'edit':
			texto_action = 'editar'
		else:
			texto_action = ''

		return render(request, 'administrador/sliders/new_edit.html', {
			'id': idR,
			'actiontype': {
				'tipo': actiontype,
				'texto': texto_action
			},
			'slider': datos
		})

def post_add_edit(request):
	if request.method == 'POST':
		validar = IsValidM(request.POST, {
			'actiontype': 'required|alpha'
		})

		if not request.user.is_authenticated:
			respuesta = {
				'code': 5,
				'boolean': False,
				'error': 'Sesión caducada',
				'result': ''
			}
		elif not validar.is_valid():
			respuesta = {
				'code': 10,
				'boolean': False,
				'error': validar.errors(),
				'result': ''
			}
		else:
			if request.POST['actiontype'] == 'add':
				validar_datos = IsValidM(request.POST, {
					'titulo': 'required|alpha_spaces_num|max:200'
				})
				validar_image = IsValidM(request.FILES, {
					'imagen': 'required'
				})

				if not validar_datos.is_valid():
					respuesta = {
						'code': 10,
						'boolean': False,
						'error': validar.errors(),
						'result': ''
					}
				elif not validar_image.is_valid():
					respuesta = {
						'code': 10,
						'boolean': False,
						'error': validar.errors(),
						'result': ''
					}
				else:
					file = request.FILES['imagen']
					fs = FileSystemStorage()
					filename = fs.save(file.name, file)
					uploaded_file_url = fs.url(filename)

					slider = Sliders()
					slider.titulo = request.POST['titulo']
					slider.image = file
					slider.activo = 'si'
					slider.save()

					respuesta = {
						'code': 0,
						'boolean': True,
						'error': '',
						'result': 'datos guardados'
					}
			elif request.POST['actiontype'] == 'edit':
				validar_datos = IsValidM(request.POST, {
					'id': 'required|numeric',
					'titulo': 'required|alpha_spaces_num|max:200'
				})

				if not validar_datos.is_valid():
					respuesta = {
						'code': 10,
						'boolean': False,
						'error': validar_datos.errors(),
						'result': ''
					}
				else:
					slider = Sliders.objects.get(id=request.POST['id'])

					if request.FILES:
						file = request.FILES['imagen']
						fs = FileSystemStorage()
						filename = fs.save(file.name, file)
						uploaded_file_url = fs.url(filename)

						del_image_path = os.path.join(settings.MEDIA_ROOT, str(slider.image))
						try:
							os.unlink(del_image_path)
						except:
							print('ocurrio un error a eliminar el archivo')

						slider.image = file

					slider.titulo = request.POST['titulo']
					slider.activo = 'si'
					slider.save()

					respuesta = {
						'code': 0,
						'boolean': True,
						'error': '',
						'result': 'datos modificados'
					}

		return JsonResponse(respuesta, safe=False)
	else:
		return redirect('slider')
		
def post_delete_slider(request, *args, **kwargs):
	print(kwargs['id'])
	if request.method == 'POST':
		validar_datos = IsValidM({'id': kwargs['id']}, {
			'id': 'required|numeric'
		})

		if not validar_datos.is_valid():
			respuesta = {
				'code': 10,
				'boolean': False,
				'error': validar_datos.errors(),
				'result': ''
			}
		else:
			slider = Sliders.objects.get(id=kwargs['id'])

			del_image_path = os.path.join(settings.MEDIA_ROOT, str(slider.image))

			try:
				os.unlink(del_image_path)
			except:
				print('ocurrio un error a eliminar el archivo')

			Sliders.objects.filter(id=kwargs['id']).delete()
			respuesta = {
				'code': 0,
				'boolean': True,
				'error': '',
				'result': 'Datos eliminados'
			}
		return JsonResponse(respuesta, safe=False)
	else:
		return redirect('slider')

class ContenidosAdmin(View):
	def get(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('/manage')
		else:
			contenidos = Contenidos.objects.all()
			return render(request, 'administrador/contenido/home.html', {'contenidos': contenidos})

class NewEditViewContenido(View):
	def get(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('/manage')

		actiontype = kwargs['actiontype']

		if 'id' in kwargs:
			idR = kwargs['id']
			datos = Contenidos.objects.get(id=idR)
			acsi = 'checked' if datos.activo == 'si' else ''
			acno = 'checked' if datos.activo == 'no' else ''
		else:
			idR = 0
			datos = None
			acsi = ''
			acno = ''

		if actiontype == 'add':
			texto_action = 'nuevo'
		elif actiontype == 'edit':
			texto_action = 'editar'
		else:
			texto_action = ''

		return render(request, 'administrador/contenido/new_edit.html', {
			'id': idR,
			'actiontype': {
				'tipo': actiontype,
				'texto': texto_action
			},
			'contenido': datos,
			'activo': {
				'si': acsi,
				'no': acno
			}
		})

def post_add_edit_contenido(request):
	if request.method == 'POST':
		validar = IsValidM(request.POST, {
			'actiontype': 'required|alpha'
		})

		if not request.user.is_authenticated:
			respuesta = {
				'code': 5,
				'boolean': False,
				'error': 'Sesión caducada',
				'result': ''
			}
		elif not validar.is_valid():
			respuesta = {
				'code': 10,
				'boolean': False,
				'error': validar.errors(),
				'result': ''
			}
		else:
			if request.POST['actiontype'] == 'add':
				validar_datos = IsValidM(request.POST, {
					'titulo': 'required|alpha_spaces_num|max:200'
				})

				if not validar_datos.is_valid():
					respuesta = {
						'code': 10,
						'boolean': False,
						'error': validar.errors(),
						'result': ''
					}
				else:
					contenido_ult = Contenidos.objects.order_by('-id')[:1]

					print(len(contenido_ult))

					if len(contenido_ult) == 0:
						orden_num = 1
					else:
						orden_num = contenido_ult[0].orden + 1

					contenido = Contenidos()
					contenido.titulo = request.POST['titulo']
					contenido.texto = request.POST['texto']
					contenido.activo = request.POST['activo']
					contenido.orden = orden_num
					contenido.save()

					respuesta = {
						'code': 0,
						'boolean': True,
						'error': '',
						'result': 'datos guardados'
					}
			elif request.POST['actiontype'] == 'edit':
				validar_datosBc = IsValidM(request.POST, {
					'id': 'required|numeric',
					'titulo': 'required|alpha_spaces_num|max:200'
				})

				if not validar_datosBc.is_valid():
					respuesta = {
						'code': 10,
						'boolean': False,
						'error': validar.errors(),
						'result': '555'
					}
				else:
					contenido = Contenidos.objects.get(id=request.POST['id'])

					contenido.titulo = request.POST['titulo']
					contenido.texto = request.POST['texto']
					contenido.activo = request.POST['activo']
					contenido.save()

					respuesta = {
						'code': 0,
						'boolean': True,
						'error': '',
						'result': 'datos modificados'
					}

		return JsonResponse(respuesta, safe=False)
	else:
		return redirect('contenidos')

def post_delete_contenido(request, *args, **kwargs):
	if request.method == 'POST':
		validar_datos = IsValidM({'id': kwargs['id']}, {
			'id': 'required|numeric'
		})

		if not validar_datos.is_valid():
			respuesta = {
				'code': 10,
				'boolean': False,
				'error': validar_datos.errors(),
				'result': ''
			}
		else:
			Contenidos.objects.filter(id=kwargs['id']).delete()
			respuesta = {
				'code': 0,
				'boolean': True,
				'error': '',
				'result': 'Datos eliminados'
			}
		return JsonResponse(respuesta, safe=False)
	else:
		return redirect('slider')
